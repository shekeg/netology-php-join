<?php
    function getTasksForAuthor($pdo, $userID, $sortBy='') {
      $sql = "SELECT t.id, t.description, t.is_done, t.date_added, t.user_id ,u.login user, au.login assigned_user 
        FROM task t JOIN user u ON t.user_id=u.id JOIN user au ON t.assigned_user_id=au.id WHERE t.user_id=:user_id";
      if ($sortBy)
        $sql .= " ORDER BY {$sortBy}";
      $sth = $pdo->prepare($sql);
      $userID = (int) $userID;
      $sth->bindParam('user_id', $userID );
      $sth->execute();
      $tasks = $sth->fetchAll(PDO::FETCH_ASSOC);
      return $tasks;
    }

    function getTasksForAssigned($pdo, $userID, $sortBy='') {
      $sql = "SELECT t.id, t.description, t.is_done, t.date_added, t.user_id ,u.login user, au.login assigned_user 
        FROM task t JOIN user u ON t.user_id=u.id JOIN user au ON t.assigned_user_id=au.id 
        WHERE t.assigned_user_id=:assigned_user_id AND t.user_id!=:assigned_user_id";
      if ($sortBy)
        $sql .= " ORDER BY {$sortBy}";
      $sth = $pdo->prepare($sql);
      $userID = (int) $userID;
      $sth->bindParam('assigned_user_id', $userID);
      $sth->execute();
      return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    function addTask($pdo, $description) {
      $sql = "INSERT INTO task (user_id, assigned_user_id, description, is_done, date_added) VALUES (:user_id, :assigned_user_id , :description, :is_done, :date_added)";
      $sth = $pdo->prepare($sql);
      $userID = (int) $_SESSION['user_id'];
      $description = strip_tags($description);
      $sth->bindParam(':user_id', $userID);
      $sth->bindParam(':assigned_user_id', $userID);
      $sth->bindParam(':description',$description);
      $sth->bindValue(':is_done', 0);
      $sth->bindValue(':date_added', date('Y-m-d h:m:s'));
      $sth->execute();
    }

    function getTask($pdo, $id) {
      $sql = "SELECT * FROM task WHERE id=:id";
      $sth = $pdo->prepare($sql);
      $id = (int) $id;
      $sth->bindParam(':id', $id);
      $sth->execute();
      $task = $sth->fetch();
      return $task;
    }

    function editTask($pdo, $id, $description) {
      $sql = "UPDATE task SET description=:description WHERE id=:id";
      $sth = $pdo->prepare($sql);
      $id = (int) strip_tags($id);
      $description = strip_tags($description);
      $sth->bindParam(':id', $id);
      $sth->bindParam(':description', $description);
      $sth->execute();
      header('Location: tasks.php');
    }

    function deleteTask($pdo, $id) {
      $sql="DELETE FROM task WHERE id=:id";
      $sth = $pdo->prepare($sql);
      $id = (int) strip_tags($id);
      $sth->bindParam(':id', $id);
      $sth->execute();
    };

    function completeTask($pdo, $id) {
      $sql="UPDATE task SET is_done=:is_done WHERE id=:id";
      $sth = $pdo->prepare($sql);
      $id = (int) strip_tags($id);
      $sth->bindParam(':id', $id);
      $sth->bindValue(':is_done', 1);
      $sth->execute();
    };

    function changeAssignUser($pdo, $id, $assignedUserID) {
      $sql = "UPDATE task SET assigned_user_id=:assigned_user_id WHERE id=:id";
      $sth = $pdo->prepare($sql);
      $id = (int) strip_tags($id);
      $assignedUserID = (int) strip_tags($assignedUserID);
      $sth->bindParam(':id', $id);
      $sth->bindParam(':assigned_user_id', $assignedUserID);
      $sth->execute();
    }