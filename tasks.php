<?php
  session_start();
  require_once(__DIR__ . '/functions_tasks.php');
  require_once(__DIR__ . '/db.php');

  if (empty($_SESSION['login'])) {
    http_response_code(403);
    die();
  }

  $login = $_SESSION['login'];
  $userID = $_SESSION['user_id'];

  if (!empty($_POST['add'])) {
    addTask($pdo, $_POST['description']);
  }
  if (!empty($_POST['save'])) {
    editTask($pdo, $_POST['id_edit'], $_POST['description_edit']);
  }
  if (!empty($_GET['action']) and $_GET['action'] === 'edit') {
    $taskForEdit = getTask($pdo, $_GET['id']);
  }
  if (!empty($_GET['action']) and $_GET['action'] === 'delete') {
    deleteTask($pdo, $_GET['id']);
  }
  if (!empty($_GET['action']) and $_GET['action'] === 'complete') {
    completeTask($pdo, $_GET['id']);
  }
  if (!empty($_POST['change_assign_user'])) {
    changeAssignUser($pdo, $_POST['task_id'], $_POST['assigned_user_id']);
  }

  if (!empty($_POST['sort'])) {
    $tasksForAuthor = getTasksForAuthor($pdo, $_SESSION['user_id'], $_POST['sort_by']);
    $taskForAssigned = getTasksForAssigned($pdo, $_SESSION['user_id'], $_POST['sort_by']);
  } else {
    $tasksForAuthor = getTasksForAuthor($pdo, $_SESSION['user_id']);
    $taskForAssigned = getTasksForAssigned($pdo, $_SESSION['user_id']);
  }
?>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Задачи</title>
  <link rel="stylesheet" href="main.css">
</head>
<body>
  <div class="wrap">
    <h2 class="title">Здравствуйте <?php echo $login ?>! Список дел:</h2>
    <?php if (!empty($_GET['action']) and $_GET['action'] === 'edit'): ?>
      <form action="tasks.php" method="POST">
        <input type="text" name="id_edit" id="id-edit" value="<?php echo $taskForEdit['id']?>" autocomplete="off" hidden>
        <label for="description-edit">Описание</label>
        <input type="text" name="description_edit" id="description-edit" value="<?php echo $taskForEdit['description']?>" autocomplete="off">
        <input type="submit" name="save" value="Сохранить">
      </form>
    <?php endif; ?>
    <?php if (empty($_GET['action']) or $_GET['action'] !== 'edit'): ?>
      <form action="tasks.php" method="POST">
        <label for="description">Описание</label>
        <input type="text" name="description" id="description" autocomplete="off">
        <input type="submit" name="add" value="Добавить">
      </form>
    <?php endif; ?>
    <form action="tasks.php" method="POST">
      <label for="sort-by">Сортировка по: </label>
      <select name="sort_by" id="sort-by" >
        <option value="date_added">Дате добавления</option>
        <option value="is_done">Статусу</option>
        <option value="description">Описанию</option>
      </select>
      <input type="submit" name="sort" value="Отсортировать">
    </form>
    <?php if (!empty($tasksForAuthor)): ?>
      <table class="table">
        <thead>
          <tr>
            <th>Описание задачи1</th>
            <th>Дата добавления</th>
            <th>Статус</th>
            <th>Действия</th>
            <th>Ответственный</th>
            <th>Автор</th>
            <th>Закрепить задачу за пользователем</th>
          </tr>
        </thead>
      <?php foreach($tasksForAuthor as $row): ?>
        <tr>
          <td><?php echo $row['description'] ?></td>
          <td><?php echo $row['date_added'] ?></td>
          <td class="<?php echo $row['is_done'] ? 'complete' : 'not-complete' ?>"><?php echo $row['is_done'] ? 'Выполнено' : 'В процессе' ?></td>
          <td>
            <a href="?action=edit&id=<?php echo $row['id'] ?>">Изменить</a>
            <a href="?action=complete&id=<?php echo $row['id'] ?>">Выполнить</a>
            <a href="?action=delete&id=<?php echo $row['id'] ?>">Удалить</a>
          </td>
          <td>
            <?php echo $row['assigned_user'] ?>
          </td>
          <td>
            <?php echo $row['user'] ?>
          </td>
          <td>
            <form action="tasks.php" method="POST">
              <input type="text" name="task_id" value="<?php echo $row['id'] ?>" hidden>
              <select name="assigned_user_id" id="assigned-user-id">
                <?php foreach($pdo->query("SELECT id, login FROM user") as $user) : ?>
                  <option value="<?php echo $user['id'] ?>"><?php echo $user['login']?></option>
                <?php endforeach; ?>
              </select>
              <input type="submit" name="change_assign_user" value="Переложить ответственность">
            </form>
          </td>
        </tr>
      <?php endforeach; ?>
      </table>
    <?php endif; ?>
    <?php if (!empty($taskForAssigned)): ?> 
      <h2>Также, посмотрите, что от Вас требуют другие люди:</h2>
      <table class="table">
        <thead>
          <tr>
            <th>Описание задачи</th>
            <th>Дата добавления</th>
            <th>Статус</th>
            <th>Действия</th>
            <th>Ответственный</th>
            <th>Автор</th>
          </tr>
        </thead>
      <?php foreach($taskForAssigned as $row): ?>
        <tr>
          <td><?php echo $row['description'] ?></td>
          <td><?php echo $row['date_added'] ?></td>
          <td class="<?php echo $row['is_done'] ? 'complete' : 'not-complete' ?>"><?php echo $row['is_done'] ? 'Выполнено' : 'В процессе' ?></td>
          <td>
            <a href="?action=edit&id=<?php echo $row['id'] ?>">Изменить</a>
            <a href="?action=complete&id=<?php echo $row['id'] ?>">Выполнить</a>
            <a href="?action=delete&id=<?php echo $row['id'] ?>">Удалить</a>
          </td>
          <td>
            <?php echo $row['assigned_user'] ?>
          </td>
          <td>
            <?php echo $row['user'] ?>
          </td>
        </tr>
      <?php endforeach; ?>
      </table>
    <?php endif; ?>
    <a href="logout.php">Выйти</a>
  </div>
</body>
</html>