<?php
  session_start();
  require_once(__DIR__ . '/db.php');
  require_once(__DIR__ . '/functions_login.php');

  if (!empty($_POST['sign_in'])) {
    if (isUser($pdo, $_POST['login'], $_POST['password'])) {
      header('Location: tasks.php');
    } else {
      echo "Указан неверный логин или пароль.";
    }
  }

  if (!empty($_POST['sign_up'])) {
    $login = $_POST['login'];
    $password = $_POST['password'];
    if (empty($login) or empty($password)) {
      echo 'Для регистрации необходимо заполнить все поля';
    } else {
      registration($pdo, $login, $password);
    }
  }

?>

<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Аутентификация</title>
</head>
<body>
  <form action="index.php" method="POST">
    <label for="login">Логин</label>
    <input type="text" name="login" id="login" autocomplete="off">
    <label for="password">Пароль</label>
    <input type="password" name="password" id="password" autocomplete="off">
    <input type="submit" name="sign_in" value="Войти">
    <input type="submit" name="sign_up" value="Заригестрироваться">
  </form>
</body>
</html>