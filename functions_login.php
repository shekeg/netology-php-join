<?php
    function isUser($pdo, $login, $password) {
      $sql = "SELECT * FROM user WHERE login=:login";
      $sth = $pdo->prepare($sql);
      $login = strip_tags($login);
      $password = strip_tags($_POST['password']);
      $sth->bindParam(':login', $login);
      $sth->execute();
      $user = $sth->fetch();
      if (empty($user)) return false;
      if (password_verify($user['password'], setSalt($password))) return false;
      $_SESSION['user_id'] = $user['id'];
      $_SESSION['login'] = $user['login'];
      $_SESSION['password'] = $user['password'];
      return true;
    }

    function registration($pdo, $login, $password) {
      $sql = "SELECT * FROM user WHERE login=:login";
      $sth = $pdo->prepare($sql);
      $login = strip_tags($login);
      $sth->bindParam(':login', $login);
      $sth->execute();
      $user = $sth->fetch();
      if (!empty($user)) {
        echo 'Пользователь с таким логином уже заригестрирован';
      } else {
        $sql = "INSERT INTO user (login, password) VALUES (:login, :password)";
        $sth = $pdo->prepare($sql);
        $sth->bindParam(':login', $login);
        $password = strip_tags($password);
        $password = password_hash(setSalt($password), PASSWORD_DEFAULT);
        $sth->bindParam(':password', $password);
        $sth->execute();
        $_SESSION['user_id'] = $pdo->lastInsertId();
        $_SESSION['login'] = $login;
        $_SESSION['password'] = $password;
        header('Location: tasks.php');
      }
    }

    function setSalt($password) {
      $salt1 = 'neto';
      $salt2 = 'logy';
      return $password = $salt1 . $password . $salt2;
    }